package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_bright.*
import kotlinx.android.synthetic.main.activity_strategy.toolbar

class Pattern5: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bright)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern5)

        btn_bright.setOnClickListener {
            var tvRemoteControl = RemoteControl(appliance = TV())
            tvRemoteControl.turnOn()
            var fancyVacuumCleanerRemoteControl = RemoteControl(appliance = VacuumCleaner())
            fancyVacuumCleanerRemoteControl.turnOn()

            bright_example.text = "${tvRemoteControl.appliance} \n${fancyVacuumCleanerRemoteControl.appliance}"
        }

    }
}

interface Switch {
    var appliance: Appliance
    fun turnOn()
}
interface Appliance {
    fun run()
}
class RemoteControl(override var appliance: Appliance) : Switch {
    override fun turnOn() = appliance.run()
}
class TV : Appliance {
    override fun run() = println("TV включен")
}
class VacuumCleaner : Appliance {
    override fun run() = println("Вакуумный очиститель включен")
}