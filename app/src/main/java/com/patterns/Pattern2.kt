package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_fabric.*

class Pattern2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabric)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern1)

        btn_fabric.setOnClickListener {
            fabric_example.text = testFabric().toString()
        }

    }
    fun testFabric()/*: CharSequence? */{
        // Cannot do this, constructor is private
        // val arya = SecretiveGirl();
        val arya1 = SecretiveGirl.newGirl("Arry")
        assertEquals(17, arya1.age)
        assertEquals("Arry", arya1.name)
        assertEquals("A girl has no desires", arya1.desires)
        val arya2 = SecretiveGirl.newGirl("Cersei Lannister", "Joffrey", "Ilyn Payne")
        assertEquals(17, arya2.age)
        assertEquals("A girl has no name", arya2.name)
        assertEquals("Cersei Lannister, Joffrey, Ilyn Payne", arya2.desires)

    }


}

class SecretiveGirl private constructor(val age: Int,
                                        val name: String = "A girl has no name",
                                        val desires: String = "A girl has no desires") {
    companion object {
        fun newGirl(vararg desires : String) : SecretiveGirl {
            return SecretiveGirl(17, desires = desires.joinToString(", "))
        }
        fun newGirl(name : String) : SecretiveGirl {
            return SecretiveGirl(17, name = name)
        }
    }
}