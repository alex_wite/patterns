package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigation = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        when (menuItem.itemId) {
            R.id.poroj -> {
                val frag = PorojFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, frag, frag.javaClass.getSimpleName())
                    .commit()

                toolbar.title = getString(R.string.poroj)

                return@OnNavigationItemSelectedListener true
            }
            R.id.structure -> {
                val fragment = StructureFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                    .commit()

                toolbar.title = getString(R.string.structure)

                return@OnNavigationItemSelectedListener true
            }
            R.id.povedenie -> {
                val fragment = PovedenieFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                    .commit()

                toolbar.title = getString(R.string.povedenie)

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.poroj)

        bottomNav.setOnNavigationItemSelectedListener(mOnNavigation)

        if (savedInstanceState == null) {
            val fragment = PorojFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                .commit()
        }

    }

    override fun onResume() {
        super.onResume()

        bottomNav.setOnNavigationItemSelectedListener(mOnNavigation)

    }

}
