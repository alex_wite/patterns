package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_iterator.*

class Pattern9: AppCompatActivity() {
    class Novella(val name: String)
    class Novellas(val novellas: MutableList<Novella> = mutableListOf()) : Iterable<Novella> {
        override fun iterator(): Iterator<Novella> = NovellasIterator(novellas)
    }
    class NovellasIterator(val novellas: MutableList<Novella> = mutableListOf(), var current: Int = 0) : Iterator<Novella> {
        override fun hasNext(): Boolean = novellas.size > current
        override fun next(): Novella {
            val novella = novellas[current]
            current++
            return novella
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iterator)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern9)

        val novellas = Novellas(mutableListOf(Novella("Test1"), Novella("Test2"), Novella("Test3")))

        btn_iterator.setOnClickListener {
            novellas.forEach { iterator_example.text = "${iterator_example.text.toString() + it.name} \n"}
        }
    }
}