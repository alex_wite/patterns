package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_adapter.*

class Pattern6: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adapter)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern6)

        btn_adapter.setOnClickListener {
            val celsiusTemperature = CelsiusTemperature(0.0)
            val fahrenheitTemperature = FahrenheitTemperature(celsiusTemperature)
            celsiusTemperature.temperature = 36.6
            var degree = "${celsiusTemperature.temperature} C -> ${fahrenheitTemperature.temperature} F\n"
            fahrenheitTemperature.temperature = 100.0
            degree += "${fahrenheitTemperature.temperature} F -> ${celsiusTemperature.temperature} C"
            adapter_example.text = degree
        }

    }
}

interface Temperature {
    var temperature: Double
}
class CelsiusTemperature(override var temperature: Double) : Temperature
class FahrenheitTemperature(var celsiusTemperature: CelsiusTemperature) : Temperature {
    override var temperature: Double
        get() = convertCelsiusToFahrenheit(celsiusTemperature.temperature)
        set(temperatureInF) {
            celsiusTemperature.temperature = convertFahrenheitToCelsius(temperatureInF)
        }
    private fun convertFahrenheitToCelsius(f: Double): Double = (f - 32) * 5 / 9
    private fun convertCelsiusToFahrenheit(c: Double): Double = (c * 9 / 5) + 32
}