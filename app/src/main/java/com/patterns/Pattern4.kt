package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_decorator.*
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class Pattern4: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_decorator)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern4)

        btn_decorator.setOnClickListener {
            decorator_example.text = testDecorator()
        }
    }
}

class HappyMap<K, V>(val map : MutableMap<K, V> = mutableMapOf()) : MutableMap<K, V> by map{
    override fun put(key: K, value: V): V? {
        return map.put(key, value).apply {
            if (this == null) {
                println("Yay! $key")
            }
        }
    }
}
fun testDecorator(): CharSequence? {
    val map = HappyMap<String, String>()
    val result = captureOutput {
        map["A"] = "B"
        map["B"] = "C"
        map["A"] = "C"
        map.remove("A")
        map["A"] = "C"
    }
    assertEquals(mapOf("A" to "C", "B" to "C"), map.map)
    assertEquals(listOf("Yay! A\n", "Yay! B\n", "Yay! A"), (result))
    return result.toString()
}

fun captureOutput(function: () -> Unit): List<String> {
    val out = System.out
    val b = ByteArrayOutputStream()

    System.setOut(PrintStream(b))

    function.invoke()

    System.setOut(out)

    return b.toString().trim().split("\r\n")
}
fun <T> assertEquals(
    expected: T,
    actual: T,
    message: String? = null
): Unit {
}

