package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_strategy.*

class Pattern7: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_strategy)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern7)

        btn_strategy.setOnClickListener {
            strateg_example.text = testStrategy()
        }

    }
}

class UncertainAnimal {
    var makeSound = fun () {
        var sound = "Мяу!!"
    }
}

fun testStrategy(): CharSequence? {
    val someAnimal = UncertainAnimal()
    val output = captureOutput {
        someAnimal.makeSound()
        someAnimal.makeSound = fun () {
            var sound = "ГАВ!!!"
        }
        someAnimal.makeSound()
    }
    return assertEquals(listOf("Мяу!!", "ГАВ!!!"), output).toString()

}