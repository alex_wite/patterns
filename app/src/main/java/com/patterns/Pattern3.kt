package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_prototype.*

class Pattern3: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prototype)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern3)


        btn_prototype.setOnClickListener {
            val bike = Bike()
            val basicBike = bike.clone()
            val advancedBike = makeJaguar(basicBike)

            prototype_example.text = "Был ${bike.model},\nа стал ${advancedBike.model}"
        }

    }

    open class Bike : Cloneable {
        private var gears: Int = 0
        private var bikeType: String? = null
        var model: String? = null
            private set

        init {
            bikeType = "Стандарт"
            model = "Леопард"
            gears = 4
        }

        public override fun clone(): Bike {
            return Bike()
        }

        fun makeAdvanced() {
            bikeType = "Продвинутый"
            model = "Ягуар"
            gears = 6
        }
    }

    fun makeJaguar(basicBike: Bike): Bike {
        basicBike.makeAdvanced()
        return basicBike
    }

}