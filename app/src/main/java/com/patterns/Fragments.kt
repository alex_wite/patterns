package com.patterns

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_poroj.*
import kotlinx.android.synthetic.main.fragment_povedenie.*
import kotlinx.android.synthetic.main.fragment_structure.*

class PorojFragment: Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  = inflater.inflate(
        R.layout.fragment_poroj, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pattern1.setOnClickListener{
                val i = Intent(this.context, Pattern1::class.java)
            context!!.startActivity(i)
        }

        pattern2.setOnClickListener{
            val i = Intent(this.context, Pattern2::class.java)
            context!!.startActivity(i)
        }

        pattern3.setOnClickListener{
            val i = Intent(this.context, Pattern3::class.java)
            context!!.startActivity(i)
        }
    }

}

class StructureFragment: Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  = inflater.inflate(
        R.layout.fragment_structure, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pattern4.setOnClickListener{
            val i = Intent(this.context, Pattern4::class.java)
            context!!.startActivity(i)
        }

        pattern5.setOnClickListener{
            val i = Intent(this.context, Pattern5::class.java)
            context!!.startActivity(i)
        }

        pattern6.setOnClickListener{
            val i = Intent(this.context, Pattern6::class.java)
            context!!.startActivity(i)
        }
    }
}

class PovedenieFragment: Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  = inflater.inflate(
        R.layout.fragment_povedenie, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pattern7.setOnClickListener{
            val i = Intent(this.context, Pattern7::class.java)
            context!!.startActivity(i)
        }

        pattern8.setOnClickListener{
            val i = Intent(this.context, Pattern8::class.java)
            context!!.startActivity(i)
        }

        pattern9.setOnClickListener{
            val i = Intent(this.context, Pattern9::class.java)
            context!!.startActivity(i)
        }

    }
}