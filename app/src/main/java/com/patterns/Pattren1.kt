package com.patterns

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_singleton.*
import java.util.*
import kotlin.system.measureTimeMillis

class Pattern1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_singleton)
        setSupportActionBar(toolbar)

        toolbar.title = getString(R.string.pattern1)

        btn_single.setOnClickListener {
            singleton_example.text = testSingleton()
        }

    }

/*    object JustSingleton {
        val value : String = "Некое значение"
    }*/

    object SlowSingleton {
        val value : String
        init {
            var uuid = ""
            val total = measureTimeMillis {
                for (i in 1..10_000) {
                    uuid = UUID.randomUUID().toString()
                }
            }
            value = "\n Вычисление завершено за $total ms \n\n $uuid"
        }
    }

    fun testSingleton(): CharSequence? {
        var singleRes = "\n Тест начат \n"
        for (i in 1..3) {
            val total = measureTimeMillis {
                singleRes += SlowSingleton.value
            }
            singleRes += "\n Заняло $total ms \n"
        }
        return singleRes
    }

}